/**
 * Implementação de um dos problemas clássicos ( Jantar dos filósofos) em C++.
 * Utilizando a classe semaforo apresentado em sala de aula.
 * Deve ter uma interface clara e objetiva do que está operando em cada problema.
 * - Projeto Final 1º Semestre SO. 
 * 
 * author: Vinicius Fernandes e Gerson Viana
 **/

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
#include<semaphore.h>
#include<pthread.h>

// Define Colors
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Define Constants
#define N 5
#define SLEEP 10
#define PENSAR 0
#define FOME 1
#define COMER 2
#define ESQUERDA (nfilosofo+4)%N //agarrar garfo da esquerda
#define DIREITA (nfilosofo+1)%N  //agarrar garfo da direita

/**
 * Escopes
 * */
void mesa(int sleep);
int menu();
void *filosofo(void *num);
void agarraGarfo(int);
void deixaGarfo(int);
void testar(int);

sem_t mutex;
sem_t S[N]; //inicializacao do semáforo
int estado[N];
int nfilosofo[N] = {0,1,2,3,4};

using namespace std;

//funcao que mostra o estado dos N filosofos
void mostrar() {
    
    for(int i = 0; i < N; i++) {       

        if (estado[i] == PENSAR) // == 0
            printf("%s O Filosofo %d esta PENSANDO ...\n", ANSI_COLOR_RESET, i);
       
        if (estado[i] == FOME) // == 1
            printf("%s O Filosofo %d esta COM FOME :(\n", ANSI_COLOR_RED, i);
       
        if (estado[i] == COMER) // == 2
            printf("%s O Filosofo %d esta COMENDO :3\n", ANSI_COLOR_GREEN, i);
    }
    printf("\n");
}

void deixaGarfo(int nfilosofo) {

    sem_wait(&mutex);
    estado[nfilosofo] = PENSAR;
    
    printf("%s *NOTICE: O Filosofo %d deixou os garfos %d e %d. E voltou a pensar. \n\n", 
        ANSI_COLOR_CYAN, nfilosofo, ESQUERDA, DIREITA);
    mostrar();

    testar(ESQUERDA);
    testar(DIREITA);
    sem_post(&mutex);
}

//funcao que testa se o filosofo pode comer
void testar(int nfilosofo) {

   if(estado[nfilosofo] == FOME && estado[ESQUERDA] != COMER && estado[DIREITA] != COMER) {

      estado[nfilosofo] = COMER;
      
      printf("%s *NOTICE: O Filosofo %d agarrou os garfos %d e %d. E começou a comer. \n\n", 
        ANSI_COLOR_CYAN, nfilosofo, ESQUERDA, DIREITA);
      mostrar();

      sem_post(&S[nfilosofo]);
   }
}

void agarraGarfo(int nfilosofo) {

   sem_wait(&mutex);
   estado[nfilosofo] = FOME;
   mostrar();
   testar(nfilosofo);
   sem_post(&mutex);
   sem_wait(&S[nfilosofo]);
}

//a thread(filosofo) espera um tempo aleatoria comendo
void comer(int i) {
    sleep(rand() % SLEEP);
}

//a thread(filosofo) espera um tempo aleatoria pensando
void pensar(int i) {
    sleep(rand() % SLEEP);
}

void *filosofo(void *num) {
    int i = *(int *) num;

    while(1) {
        pensar(i);
        agarraGarfo(i);
        comer(i);
        deixaGarfo(i);
    }
}

int main() {

    if (menu() != 1)
        exit(EXIT_SUCCESS);

    system("clear");
    printf("\n");

    int i;
    int res;
    pthread_t thread_id[N]; //identificadores das threads

    for(i = 0; i < N; i++)
        estado[i] = PENSAR;

    printf("%s O JANTAR SERÁ SERVIDO EM BREVE ... \n\n", ANSI_COLOR_GREEN);
    sleep(2);
    mostrar();

    sem_init(&mutex, 0, 1);

    for(i = 0; i < N; i++)
        sem_init(&S[i], 0, 0);

    for(i = 0; i < N; i++) {

        res = pthread_create(&thread_id[i], NULL, filosofo, &nfilosofo[i]); //criar as threads
        if (res != 0) {
            perror("Erro na inicialização da thread!");
            exit(EXIT_FAILURE);
        }
    }

    for(i = 0; i < N; i++) {

        res = pthread_join(thread_id[i],NULL); //para fazer a junção das threads
        if (res != 0) {
            perror("Erro ao fazer join nas threads!");
            exit(EXIT_FAILURE);
        }
    }

    return(0);
}

void mesa(int sleep){
	system("clear");
    cout << endl;
    cout << ANSI_COLOR_CYAN;
    cout << "                                .-:/:/::::/://-.`                               " << endl; usleep(sleep);
    cout << "                           ./////-.`           `-:////-`                        " << endl; usleep(sleep);
    cout << "                       .///-`        `.::::-.        `.///-                     " << endl; usleep(sleep);
    cout << "                    .++:`         `:++/+/////+/-          .++:`                 " << endl; usleep(sleep);
    cout << "                  :o:`           /o:++s////++///o-           -++`               " << endl; usleep(sleep);
    cout << "               `+o.             o+:+:+o+//:s//++:s-            `/o-             " << endl; usleep(sleep);
    cout << "              :o.              :o:+://o/+++++://+-y              `/+`           " << endl; usleep(sleep);
    cout << "            .o-                /+/+//:++////:/::+-y.               `o:          " << endl; usleep(sleep);
    cout << "           :o`            `:`  :o:++o/++++/+/::o+-y   -.             :o`        " << endl; usleep(sleep);
    cout << "          +/        ``     -+-  o////:::///:/+++/o: `/:     ```       .s`       " << endl; usleep(sleep);
    cout << "         o/    .:+++//++/:. `/:.`+o:///:////+///s-`-+.  -/++++/++/-`   .s`      " << endl; usleep(sleep);
    cout << "        //   -++//+///:/+/++-`/+. ./++///+///++:``//-`:++/++//:///++/`  .s`     " << endl; usleep(sleep);
    cout << "       .s   /o://o+:/:::o/o/o/``    `.-:///:-.`    ``o////o+::/+/+o//o.  :o     " << endl; usleep(sleep);
    cout << "       y`  :o-o//+o+/://:/o+:o:                     o//+::o//++/++/++:s`  s-    " << endl; usleep(sleep);
    cout << "      :o   s-//:+/+o///+///+::s                    .y-//::/o+/++///:s-+:  .s    " << endl; usleep(sleep);
    cout << "      s.   s:++o+///+//o::///:s                    .y://:/+/////+++++-o:   y`   " << endl; usleep(sleep);
    cout << "      h    :o:o/++/+//+/:::+-o:                     s//+//:/++/////o+-s`   +-   " << endl; usleep(sleep);
    cout << "     `y     ++:+////+o:://:/++                      `o////+/:::::/++/o-    :+   " << endl; usleep(sleep);
    cout << "     `s      -+++//o////:/++-                        `/o/:+/+o+:///o/`     :o   " << endl; usleep(sleep);
    cout << "     `y       `.:+/o/++/+:.                            `-/+++///+/:.       :/   " << endl; usleep(sleep);
    cout << "      y           ``....-:`                             -:....``           o-   " << endl; usleep(sleep);
    cout << "      o-          `.-::::/.````                     ````-:::::-.           h    " << endl; usleep(sleep);
    cout << "      .s          ..`` .:/+/+//+/-`             .://+//+//-` ``.`         -o    " << endl; usleep(sleep);
    cout << "       s-            .++/+/+/:///++/`         .++//:////:/++/`            y`    " << endl; usleep(sleep);
    cout << "       `y`          /+:/o+:/+//:+/:/o.       /+:+//://::++++/o-          +/     " << endl; usleep(sleep);
    cout << "        -o         ++:+:/:/++/+::::/:s`     /o:/+:/:+//++///+:s.        :o      " << endl; usleep(sleep);
    cout << "         :o`      `y:+/::/:+//++:::+:+/ :-. y:++////+/+++//:/:/o       -s`      " << endl; usleep(sleep);
    cout << "          -o`     `y-o+:+o+++/o+/+:/:++ /:. y-o/-:+/+/o+/+o/+:/o      /+        " << endl; usleep(sleep);
    cout << "           .s-     o/:+o+//+//+::o++:s. -/  ++:o/-///++//+++o:o:    `o/         " << endl; usleep(sleep);
    cout << "            `++`   `o//o///++o///++/o:  -/  `o+//:/:/+o///++:o/    -o.          " << endl; usleep(sleep);
    cout << "              .o/`  `/o///+o+/////++.   `:    :+//+/++//++/++-   -o:            " << endl; usleep(sleep);
    cout << "                -+:`  `:++/////++/.             -+o+::/:+o/.  `-o/`             " << endl; usleep(sleep);
    cout << "                  .++-`    ```                      ```     `/+:                " << endl; usleep(sleep);
    cout << "                     :/+-`                               .///`                  " << endl; usleep(sleep);
    cout << "                        .///:.                      `-///:`                     " << endl; usleep(sleep);
    cout << "                            .://///:-..`````.-://////-                          " << endl; usleep(sleep);
    cout << "                                   `..-:::::...                                 " << endl; usleep(sleep);
}

int menu() {
    int start = 0;

	mesa(50000);

    cout << ANSI_COLOR_CYAN << "\n  [ JANTAR DOS FILÓSOFOS ]" << endl;
    cout << ANSI_COLOR_GREEN << "\n  ➸  O Jantar está na Mesa. Podemos começar? [1 - SIM] :> ";
    cin >> start;

    return start;
}

